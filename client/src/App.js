import React from 'react';
import logo from './logo.svg';
import './App.css';
import Header from "./components/Header";
import {Switch, BrowserRouter as Router, Route, Link} from 'react-router-dom';
import Table from "./components/Body/Table";
import Loader from "./components/Items/Loader";
import {TableBody} from "./components/Body/Table/TableBody";
import {Routes} from './routes/index'

// import Footer from './components/Footer/index';
function App() {
    return (
        <Router>
                <React.Fragment>
                    <div className="App">
                        <Routes/>
                    </div>
                </React.Fragment>
        </Router>
    );
}

export default App;
