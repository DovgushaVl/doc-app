import React from "react";
import './ErrorAlert.css'
import {useStore} from "react-redux";
import {connect} from "react-redux";
import {showError} from "../../redux/actions";
import {DELETE_ERROR} from "../../redux/types";

const ErrorAlert = (props) => {
    console.log(props);
    const errStatus = props.data.data.status || 300;
    const errMessage = props.data.data.message;
    const style = props.data.error;
    const showModal = (style) => {
        if (style === true) {
            return {display: 'flex'};
        } else {
            return {display: 'none'}
        }
    };
    const onBtnClick = ()=>{
        props.showError({type:DELETE_ERROR});
    };
    return (
        <div className="modalDialog" style={showModal(style)}>
            <div id="errorAlert">
                <h1 id="pErrorAlert">Ошибка</h1>
                <div className="errorStatus">Статус ошибки ->    {errStatus}</div>
                <div className="errorMessage">{errMessage}</div>
                <button className="errorAlertBtn btn btn-primary" type={"button"} onClick={onBtnClick}>Ok</button>
            </div>
        </div>)
};
const mapStateToProps = state => {
    return {data : state.error}
};
const mapDispatchToProps = {
    showError
};
export default connect(mapStateToProps,mapDispatchToProps)(ErrorAlert)