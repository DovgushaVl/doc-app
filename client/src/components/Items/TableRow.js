import React from "react";
import './table.css';

export const TableRow = (props) => {
    const data = props.data;

    const index = props.index + 1;
    let template = {
        entryNumb: '-',
        registrNumber: '-',
        adressee: '-',
        description: '-',
        executor: '-',
        outputNumber: '-',
        replyOn: '-'
    };

    const filteredData = {...template, ...data};
    return (
        <tr id={filteredData._id}>
            <th scope="row" data-id={"index"}>{index}</th>
            <td data-id={"entryNumb"}>{filteredData.entryNumb}</td>
            <td data-id={"registrNumber"}>{filteredData.registrNumber}</td>
            <td data-id={"adressee"}>{filteredData.adressee}</td>
            <td data-id={"description"}>{filteredData.description}</td>
            <td data-id={"executor"}>{filteredData.executor}</td>
            <td data-id={"outputNumber"}>{filteredData.outputNumber}</td>
            <td data-id={"replyOn"}>{filteredData.replyOn}</td>
        </tr>

    )
};

// return (
//     <tr id={filteredData.id}>
//         <th scope="row">{index}</th>
//         <td id={filteredData.entryNumb}>{filteredData.entryNumb}</td>
//         <td id={filteredData.registrNumber}>{filteredData.registrNumber}</td>
//         <td id={filteredData.adressee}>{filteredData.adressee}</td>
//         <td id={filteredData.description}>{filteredData.description}</td>
//         <td id={filteredData.executor}>{filteredData.executor}</td>
//         <td id={filteredData.outputNumber}>{filteredData.outputNumber}</td>
//         <td id={filteredData.reply}>{filteredData.reply}</td>
//     </tr>
// )