import React from "react";
import './successAlert.css'
import {connect} from "react-redux";
import {successAlert} from "../../redux/actions";
import {DELETE_MESSAGE} from "../../redux/types";

const ErrorAlert = (props) => {
    console.log(props);
    const message = props.data.data.text;
    console.log(props);
    const style = props.data.message;
    const showModal = (style) => {
        if (style === true) {
            return {display: 'flex'};
        } else {
            return {display: 'none'}
        }
    };
    const onBtnClick = () => {
        console.log(DELETE_MESSAGE);
        props.successAlert({type: DELETE_MESSAGE});
    };
    return (
        <div className="modalDialog" style={showModal(style)}>
            <div id="successAlert">
                <h1 id="pSuccessAlert">{message}</h1>
                <button className="successAlertBtn btn btn-primary" type={"button"} onClick={onBtnClick}>Ok</button>
            </div>
        </div>)
};
const mapStateToProps = state => {
    return {data: state.success}
};
const mapDispatchToProps = {
    successAlert
};
export default connect(mapStateToProps, mapDispatchToProps)(ErrorAlert)