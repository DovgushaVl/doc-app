import React from "react";
import {connect} from 'react-redux';
import './loader.css'
const Loader = (props) => {
    const loading = props.loading;
    const style = () => {
        if (loading === false){
            return {display:'none'};
        }
        else {
            return {display: 'flex'}
        }
    };
    // return (<div className="loader" id="loader" style={style()}></div>);
    return (<div id="loaderRoot"  style={style()}>
        <div className="spinner-border text-primary" role="status" id="loader">
        <span className="sr-only">Loading...</span>
    </div></div>);

};
const mapStateToProps = (state) => {
    return {loading: state.loader.loading};
};

export default connect(mapStateToProps, null)(Loader);