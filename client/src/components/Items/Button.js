import React, {Component} from "react";
export default class Button extends Component {
    constructor(props) {
        super();
        this.props = props;
    }

    render() {
        return (
            <>
                <button className={this.props.className} type={this.props.type} onClick={this.props.onClick} id={this.props.id}>{this.props.value}</button>

            </>)
    }

}