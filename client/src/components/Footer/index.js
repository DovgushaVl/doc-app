import React, {Component} from "react";
import API from "../../services/API";
import Loader from "../Items/Loader";
import {SHOW_LOADER, HIDE_LOADER, FIND_DOC} from "../../redux/types";
import {connect} from "react-redux";
import {searchDoc, showLoader} from "../../redux/actions";

const loading = {type: SHOW_LOADER, payload: true};
const loaded = {type: HIDE_LOADER, payload: false};
const Footer = (props) => {
    const productId = props.match.params.id;
    console.log('props',props);
    const componentMount = (async function () {
        props.showLoader(loading);
        const Api = new API();
        const res = await Api.getDoc(`/search/income/text=${productId}`).then(data => {
            return data;
        });
        // console.log(1);
        // props.searchDoc2(`/search/income/text=${productId}`);
        // console.log(2);

        // console.log(searchDoc2(`/search/income/text=${productId}`));
        const action = {data : res};
        props.searchDoc(action);
        props.asd(action);
        props.showLoader(loaded);

        console.log(props, 'res--->', await res);
    })();
    return (<div>{productId}</div>)
};
// const mapDispatchToProps = {
//     searchDoc,showLoader
// };
const mapDispatchToProps = (dispatch) => {
    return {
        asd: (data) => {
            dispatch(searchDoc(data))
        },

        searchDoc: (data) => {
            dispatch(searchDoc(data))
        },
        showLoader: (data) => {
            dispatch(showLoader(data));
        },
        // searchDoc2 : (url)=> {
        //     dispatch(searchDoc2(url))
        // }
    }
};

export default connect(null, mapDispatchToProps)(Footer);