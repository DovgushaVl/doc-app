import React, {useEffect, useState} from "react";
import {connect} from "react-redux";
import {useParams} from 'react-router-dom';
import {searchDoc} from "../../../../redux/actions";
import {TableRow} from "../../../Items/TableRow";
import './../style.css'
const TableBody = (props) => {
const params = useParams();
useEffect(()=>{
    const select = params.select;
    const searchFieldValue = params.searchFieldValue;
    return props.searchDoc({select,searchFieldValue})
    ;},[params]);
if (props.data.length < 1){
    return (<tbody>
    <tr>
        <td colSpan={8}>
            Нічого не знайдено</td>
    </tr>

    </tbody>);
}
return (
    <tbody id="tbody">
            {props.data.map(item => {
                return (<TableRow data={item} key={item._id} index={props.data.indexOf(item)}/>)
            })}
    </tbody>
);
};
const mapDispatchToProps = {
    searchDoc
};
const mapStateToProps = state => {
    return state.searchResult
};
export default connect(mapStateToProps, mapDispatchToProps)(TableBody);