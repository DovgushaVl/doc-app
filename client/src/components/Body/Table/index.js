import React, {Component, useState} from "react";
import {BrowserRouter, Route} from 'react-router-dom';
import './style.css';
import TableBody from "./TableBody";

export const Table = (props) => {
    return (
        <div id="tableWrapper">       <table className="table" id="table">
                        <thead>
                        <tr id="thead">
                            <th scope="col" data-id={"index"}>#</th>
                            <th scope="col" data-id={"entryNumb"}>Вход. номер</th>
                            <th scope="col" data-id={"registrNumber"}>Рег номер</th>
                            <th scope="col" data-id={"adressee"}>Адресат</th>
                            <th scope="col" data-id={"description"}>Описание</th>
                            <th scope="col" data-id={"executor"}>Исполнитель</th>
                            <th scope="col" data-id={"outputNumber"}>Вых номер</th>
                            <th scope="col"  data-id={"replyOn"}>Ответ на</th>
                        </tr>
                        </thead>
                            <Route exact path="/search/:select/:searchFieldValue"><TableBody/></Route>
                    </table>
        </div>

    )
};
// const mapStateToProps = state => {
//     return {
//         searchResult: state.searchResult
//     }
//
// };
// export default connect(mapStateToProps, null)(Table);