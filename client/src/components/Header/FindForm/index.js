import React from "react";
import {connect} from "react-redux";
import './style.css'
import {searchDoc, showLoader, showError} from './../../../redux/actions'
import {ADD_ERROR} from "../../../redux/types";


const FindForm = (props) => {
    const onSubmit = async function (e) {
        e.preventDefault();
        const data = {};
        const target = e.target.id;
        data.target = target;
        const formData = {};
        const formElems = e.target.children;
        for (let elem of formElems) {
            if (elem.tagName !== 'BUTTON') {
                const id = elem.id;
                const value = elem.value;
                formData[id] = value;
            }
        }
        data.formData = formData;
        if (data.formData.searchField.length < 3) {
            props.showError({type:ADD_ERROR, payload : {message:"Мiнiмальна кiлькiсть символiв для пошуку - 3!"}});
        } else {
            const select = data.formData.searchSelect;
            const searchFieldValue = data.formData.searchField;
            return props.history.push(`/search/${select}/${searchFieldValue}`)
        }
    };

    return (
        <form id="searchForm" onSubmit={onSubmit}>
            <button className="btn btn-primary" type="submit" id='searchBtn'>Знайти</button>
            <select name="searchSelect" id="searchSelect">
                <option value="income">Вхiдний документ</option>
                <option value="outcome">Вихiдний документ</option>
                <option value="any">Шукати скрiзь</option>
            </select>
            <input type="text" className="searchInput" placeholder="Пошук" aria-label="Пошук"
                   key="searchField"
                   id="searchField"
                   title="Мiнiмальна кiлькiсть символiв для пошуку - 3"/>
        </form>)
};
const mapDispatchToProps = {
    searchDoc, showLoader, showError
};


export default connect(null, mapDispatchToProps)(FindForm);


// const mapDispatchToProps = (dispatch) => {
//     return {
//         showError: (data) => {
//             dispatch(showError(data))
//         },
//
//         searchDoc: (data) => {
//             dispatch(searchDoc(data))
//         },
//         showLoader: (data) => {
//             dispatch(showLoader(data));
//         },
//         // searchDoc2 : (url)=> {
//         //     dispatch(searchDoc2(url))
//         // }
//     }
// };

// const onSubmit = async function (e) {
//     e.preventDefault();
//     const data = {};
//     const target = e.target.id;
//     data.target = target;
//     const formData = {};
//     const formElems = e.target.children;
//     for (let elem of formElems) {
//         if (elem.tagName !== 'BUTTON') {
//             const id = elem.id;
//             const value = elem.value;
//             formData[id] = value;
//         }
//     }
//     data.formData = formData;
//     if (data.formData.searchField.length < 3) {
//         const Err = {show: true};
//         setState({...state, ...Err});
//     } else {
//         const select = data.formData.searchSelect;
//         const searchFieldValue = data.formData.searchField;
//         props.showLoader(loading);
//         const Api = new API();
//         const fetchedData = await Api.getDoc(`/search/${select}/text=${searchFieldValue}`).then(res => {
//             if (res.ok) {
//                 return res.json().then(data => {
//                     props.searchDoc({data:data});
//                 });
//             } else {
//                 const error = {status: res.status, data: res.statusText};
//                 props.showError({type:ADD_ERROR, payload:error});
//                 props.searchDoc({data:[]})
//             }
//         });
//         props.showLoader(loaded);
//     }
// };