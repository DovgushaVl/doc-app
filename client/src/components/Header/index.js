import React, {Component} from "react";
import './style.css'
import Menu from "./Menu";
import FindForm from "./FindForm";
import OutcomeForm from "./AddDocument/OutcomeForm";
import IncomingForm from "./AddDocument/IncomingForm";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import {notFound} from '../Items/NotFound';
import TableBody from "../Body/Table/TableBody";

export default class Header extends Component {
    constructor() {
        super();
        this.state = {incoming: {display: 'none'}, outcome: {display: 'none'}};
    }

    render() {
        return (
            <header className="App-header" id="Header">
                <FindForm {...this.props}/>
                <Menu/>
                <IncomingForm/>
                <OutcomeForm/>
            </header>)
    }
}