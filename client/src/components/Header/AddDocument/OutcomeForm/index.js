import React, {useState} from "react";
import './style.css'
import {showForm} from "../../../../redux/actions";
import {SHOW_FORM, HIDE_FORM, ADD_MESSAGE, ADD_ERROR} from "../../../../redux/types";
import {connect, useDispatch} from 'react-redux';
import {baseUrl, loaded, loading} from "../../../../const";
import API from "../../../../services/API";

const OutcomeForm = (props) => {
    const style = props.style.show;
    const dispatch = useDispatch();
    const defaultState = {
        outputNumber: '',
        adressee: '',
        description: '',
        executor: '',
        replyOn: ''
    };
    const [state, setState] = useState(defaultState);

    const onSubmit = async (e) => {
        e.preventDefault();
        const formId = e.target.id;
        const formData = {};
        const target = e.target.id;
        const childArr = e.target.children;
        for (let elem of childArr) {
            if (elem.tagName === 'INPUT' || elem.tagName === 'TEXTAREA') {
                formData[elem.id] = elem.value;
            }
        }
        dispatch(loading);
        const Api = new API();
        const res = await Api.addDoc(`${baseUrl}/create/${formId}/`, state).then(res => {
            if (res.ok) {
                return res.json().then(data => {
                    dispatch(loaded);
                    dispatch({
                        type: HIDE_FORM,
                        payload: formId
                    });
                    dispatch({type: ADD_MESSAGE, payload: data});

                    return setState(data => {
                        setState(data => {
                            return defaultState
                        });
                    });
                });
            } else {
                console.log(res);
                try {
                    return res.json().then(data => {
                        console.log(data.error);
                        dispatch({type: ADD_ERROR, payload: data.error});
                        dispatch(loaded);
                    })
                } catch (e) {
                    dispatch(loaded);
                    dispatch({type: ADD_ERROR, payload: {status: 500, message: res.stack}});
                }
            }
        });
    };
    const showModal = (style) => {
        console.log(style);
        if (style === true) {
            return {display: 'flex'}
        } else {
            return {display: 'none'}
        }
    };
    const onClickCloseBtn = (e) => {
        const target = e.target.parentNode.id;
        const data = {
            type: HIDE_FORM,
            payload: target
        };
        props.showForm(data);
    };
    const onChange = (e) => {
        const value = e.target.value;
        const target = e.target.id;
        setState(data => {
            return {...state, ...{[target]: value}}
        });
    };
    console.log(state);
    return (
        <div className="modalDialog" style={showModal(style)}>
            <form id="outcome" onSubmit={onSubmit} className="outcomeForm">
                <button className='closeBtn btn btn-primary' type="button" onClick={onClickCloseBtn}>
                    X
                </button>
                <label htmlFor='outputNumber'>
                    Вихідний номер
                </label>
                <input type="text" className="outputNumber" placeholder="Вихідний номер" aria-label="" id="outputNumber"
                       title=""
                       onChange={onChange}
                       value={state.outputNumber}

                />
                <label htmlFor='adressee'>
                    Адресат
                </label>
                <input type="text" className="adressee" placeholder="Адресат" aria-label="" id="adressee"
                       title=""
                       onChange={onChange}
                       value={state.adressee}
                />
                <label htmlFor='description'>
                    Опис
                </label>
                <textarea className="description" placeholder="Опис" aria-label=""
                          id="description"
                          title=""
                          value={state.description}
                          onChange={onChange}
                />
                <label htmlFor='executor'>
                    Виконавець</label>
                <input type="text" className="executor" placeholder="Виконавець" aria-label=""
                       id="executor"
                       title=""
                       value={state.executor}
                       onChange={onChange}
                />


                <label htmlFor='replyOn'>
                    Відповідь на</label>
                <input type="text" className="replyOn" placeholder="Виконавець" aria-label=""
                       id="replyOn"
                       title=""
                       value={state.replyOn}
                       onChange={onChange}
                />
                <div id="submBtnWrapper">
                    <button id="formSubmitBtn" className="btn btn-primary" type={"submit"}>Зберегти</button>
                </div>
            </form>
        </div>)
};

const mapStateToProps = state => {
    return {style: state.form.outcome};
};
const mapDispatchToProps = {
    showForm
};

export default connect(mapStateToProps, mapDispatchToProps)(OutcomeForm);