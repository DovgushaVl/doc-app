import React, {useState} from "react";
import './style.css';
import {connect} from 'react-redux';
import {showForm} from "../../../../redux/actions";
import {ADD_ERROR, ADD_MESSAGE, FIND_DOC, HIDE_FORM} from "../../../../redux/types";
import {useDispatch} from 'react-redux';
import API from "../../../../services/API";
import {baseUrl, loaded, loading} from "../../../../const";


const IncomingForm = (props) => {
    const dispatch = useDispatch();
    const defaultState = {
        entryNumb: '',
        registrNumber: '',
        adressee: '',
        description: '',
        executor: '',
        outputNumber: '',
        data: ''
    };
    const [state, setState] = useState(defaultState);
    const onSubmit = async (e) => {
        console.log(state);
        let data = '';
        for (let item in state) {
            console.log(item);
            data += state[item] + " ";
        }
        console.log(data);
        setState((state) => {
            return {...state, ...{data: data}}
        });
        console.log(state);
        e.preventDefault();
        const formId = e.target.id;
        const formData = {};
        const target = e.target.id;
        const childArr = e.target.children;
        for (let elem of childArr) {
            if (elem.tagName === 'INPUT' || elem.tagName === 'TEXTAREA') {
                formData[elem.id] = elem.value;
            }
        }
        dispatch(loading);
        const Api = new API();
        const res = await Api.addDoc(`${baseUrl}/create/${formId}/`, state).then(res => {
            if (res.ok) {
                return res.json().then(data => {
                    dispatch(loaded);
                    // dispatch({
                    //     type: HIDE_FORM,
                    //     payload: formId
                    // });
                    // dispatch({type: ADD_MESSAGE, payload: data});

                    // return setState(data => {
                    //     setState(data => {
                    //         return defaultState
                    //     });
                    // });
                });
            } else {
                try {
                    return res.json().then(data => {
                        console.log(data.error);
                        dispatch({type: ADD_ERROR, payload: data.error});
                        dispatch(loaded);
                    })
                } catch (e) {
                    dispatch(loaded);
                    dispatch({type: ADD_ERROR, payload: {status: 500, message: res.stack}});
                }
            }
        });
    };
    const style = props.style.show;
    const showModal = (style) => {
        if (style === true) {
            return {display: 'flex'};
        } else {
            return {display: 'none'}
        }
    };
    const onClickCloseBtn = (e) => {
        const formId = e.target.parentNode.id;
        console.log(formId);
        const data = {
            type: HIDE_FORM,
            payload: formId
        };
        props.showForm(data);
        setState(data => {
            return defaultState
        });
        console.log(state);
    };
    const onChange = (e) => {
        const value = e.target.value;
        const target = e.target.id;
        setState(data => {
            return {...state, ...{[target]: value}}
        });
        console.log(state);
    };

    return (
        <div className="modalDialog" style={showModal(style)}>

            <form id="incoming" onSubmit={onSubmit} className="incomeForm">
                <button className='closeBtn btn btn-primary' type="button" onClick={onClickCloseBtn}>
                    X
                </button>
                <label htmlFor='entryNumb'>
                    Вхідний номер
                </label>
                <input type="text" className="entryNumb" placeholder="Вхідний номер" aria-label="" id="entryNumb"
                       title="" onChange={onChange}
                       value={state.entryNumb}
                />
                <label htmlFor='registrNumber'>
                    Регістровий номер
                </label>
                <input type="text" className="registrNumber" placeholder="Регістровий номер" aria-label=""
                       id="registrNumber"
                       title="" onChange={onChange}
                       value={state.registrNumber}
                />
                <label htmlFor='adressee'>
                    Адресат
                </label>
                <input type="text" className="adressee" placeholder="Адресат" aria-label="" id="adressee"
                       title="" onChange={onChange}
                       value={state.adressee}
                />
                <label htmlFor='descriprion'>
                    Опис
                </label>
                <textarea className="description" placeholder="Опис" aria-label=""
                          id="description"
                          title="" onChange={onChange}
                          value={state.description}
                />
                <label htmlFor='executor'>
                    Виконавець</label>
                <input type="text" className="executor" placeholder="Виконавець" aria-label=""
                       id="executor"
                       title="" onChange={onChange}
                       value={state.executor}
                />
                <label htmlFor='outputNumber'>
                    Вихідний номер
                </label>

                <input type="text" className="outputNumber" placeholder="Вихідний номер"
                       aria-label="" id="outputNumber"
                       title="" onChange={onChange}
                       value={state.outputNumber}
                />
                <div id="submBtnWrapper">
                    <button id="formSubmitBtn" className="btn btn-primary" type={"submit"}>Зберегти</button>
                </div>
            </form>
        </div>
    )
};
const mapStateToProps = state => {
    return {style: state.form.incoming}
};
const mapDispatchToProps = {
    showForm
};
export default connect(mapStateToProps, mapDispatchToProps)(IncomingForm);

