import React, {Component} from "react";
import Button from "../../Items/Button";
import './style.css';
import {connect} from "react-redux";
import {showForm} from "../../../redux/actions";
import {SHOW_FORM} from "../../../redux/types";

class Menu extends Component {
    constructor(props) {
        super(props);
        this.props = props;
        this.state = {
            menuList: {display: 'none'},
            incoming: {className: "dropdown-item", type: "button", id: "incoming", value: "Добавить входящий документ"},
            outcome: {
                className: "dropdown-item",
                type: "button",
                id: "outcome",
                value: "Добавить исходящий документ"
            }
        };
        this.onClickMenu = this.onClickMenu.bind(this);
        this.onClickBtn = this.onClickBtn.bind(this);
        this.onBlurBtn = this.onBlurBtn.bind(this);
    }

    onClickMenu(e) {
        const target = e.target.parentNode.lastChild;
        console.dir(e.target);
        const oldState = {...this.state};
        if (this.state.menuList.display === 'none') {
            const newState = {menuList: {display: 'flex'}};
            return this.setState({...oldState, ...newState});
        }
        const newState = {menuList: {display: 'none'}};
        return this.setState({...oldState, ...newState})
    }

    onClickBtn(e) {
        const newState = {menuList: {display: 'none'}};
        const oldState = {...this.state};
        const target = e.target.id;
        const data = {
            type: SHOW_FORM,
            payload: target
        };
        this.props.showForm(data);

        this.setState({...oldState, ...newState})
    }
    onBlurBtn(e){
        console.dir(e.target);

    }
    render() {

        return (
            <div className="btn-group" id="btn-group">
                <button type="button" className="btn dropdown-toggle btn btn-primary" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false" id="Menu" onClick={this.onClickMenu}
                        onBlur={this.onBlurBtn}>
                    Menu
                </button>
                <div className="dropdown-menu dropdown-menu-right" id="menuList" style={this.state.menuList}>
                    <button className={this.state.incoming.className}
                            type={this.state.incoming.type}
                            id={this.state.incoming.id}
                            onClick={this.onClickBtn}
                    >
                        {this.state.incoming.value}
                    </button>
                    <button className={this.state.outcome.className} type={this.state.outcome.type}
                            id={this.state.outcome.id}
                            onClick={this.onClickBtn}
                    >{this.state.outcome.value}</button>
                </div>
            </div>);
    }

}

const mapDispatchToProps = {
    showForm
};
export default connect(null, mapDispatchToProps)(Menu);