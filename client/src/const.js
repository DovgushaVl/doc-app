import {DELETE_ERROR, DELETE_MESSAGE, HIDE_LOADER, SHOW_LOADER} from "./redux/types";
const loading = {type: SHOW_LOADER, payload: true};
const loaded = {type: HIDE_LOADER, payload: false};
const baseUrl = 'http://localhost:3001';
// const baseUrl = 'http://192.168.0.66:3001'; // IP сервера

const deleteError = {type: DELETE_ERROR, payload: false};
const deleteMessage = {type: DELETE_MESSAGE, payload: false};
export {loading,loaded,baseUrl};
