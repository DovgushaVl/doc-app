import React from 'react';
import {Switch, Route, BrowserRouter} from 'react-router-dom';
import Header from "../components/Header";
import {Table} from "../components/Body/Table";
import {TableBody} from "../components/Body/Table/TableBody";
import {TableRow} from "../components/Items/TableRow";
import FindForm from "../components/Header/FindForm";
import Menu from "../components/Header/Menu";
import IncomingForm from "../components/Header/AddDocument/IncomingForm";
import OutcomeForm from "../components/Header/AddDocument/OutcomeForm";
import Loader from "../components/Items/Loader";
import SuccessAlert from "../components/Items/SuccessAlert";
import ErrorAlert from "../components/Items/ErrorAlert";

export const Routes = () => {
    return (
        <BrowserRouter>
            <Switch>
                <React.Fragment>
                    <Route path={'/'} component={props => <Header {...props}/>}></Route>
                    <Route path={'/'} component={props => <Table {...props}/>}></Route>
                    <Route path={'/'} component={props => <Loader/>}></Route>
                    <Route path={'/'} component={SuccessAlert}></Route>
                    <Route path={'/'} component={ErrorAlert}></Route>
                </React.Fragment>
            </Switch>
        </BrowserRouter>
    )
};