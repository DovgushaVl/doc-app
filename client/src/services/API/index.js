import {connect} from "react-redux";
import {showError} from "../../redux/actions";

export default class API {
    async getDoc(url) {
        try {
            // const res = await fetch("https://jsonplaceholder.typicode.com/todos/30",
            const res = await fetch(url,
                {
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8',
                        'Content-Security-Policy': 'default-src self',
                        "Accept": "application/json",
                        "Access-Control-Allow-Origin": "*",
                        "X-Requested-With": "XMLHttpRequest",
                        "Access-Control-Allow-Methods": "GET,POST,PUT,DELETE,OPTIONS"
                    }
                });

            return res;
        } catch (e) {
            console.log(e);
            return e;
        }
    }
    async addDoc(url,data) {
        try {
            console.log(JSON.stringify(data));
            const res = await fetch(url, {method: 'POST',     headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },// или 'PUT'
                body: JSON.stringify(data)});
            return res;
        }catch (e) {
            console.log(e);
            return e;
        }
    }
}
