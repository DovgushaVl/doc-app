import {ADD_ERROR, DELETE_ERROR, FIND_DOC} from "./types";

const initialState = {
    error : false,
    data: {}
};
export const errorReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_ERROR : {
            return {...state, error: true,data: action.payload}
        }
        case (DELETE_ERROR) : {
            return {...state, error: false, data: {}}
        }
    }
    return state;
};

