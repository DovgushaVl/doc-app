import {combineReducers} from "redux";
import {formReducer} from "./formReducer";
import {findFormReducer} from './findReducer';
import {loaderReducer} from "./loaderReducer";
import {errorReducer} from "./errorReducer";
import {successReducer} from "./successReducer";

export const rootReducer = combineReducers({form:formReducer,searchResult:findFormReducer, loader:loaderReducer,error:errorReducer,success:successReducer});