import {ADD_ERROR, ADD_MESSAGE, FIND_DOC} from "./types";
import API from "../services/API";
import {loading, loaded} from "../const";
import {baseUrl} from "../const";

export function searchDoc({select, searchFieldValue}) {
    return function (dispatch) {
        // dispatch(loading);
        dispatch(loading);
        const Api = new API();
        const fetchedData = Api.getDoc(`${baseUrl}/search/${select}/text=${searchFieldValue}`).then(res => {
            if (res.ok) {
                return res.json().then(data => {
                    dispatch({type: FIND_DOC, payload: data});
                    dispatch(loaded);
                    return true;
                });
            } else {
                console.log(res);
                try{
                res.json().then(data=> {
                    const error = data.error;
                    dispatch({type: ADD_ERROR, payload: error});
                    dispatch(loaded);})
                }catch (e) {
                    dispatch(loaded);
                    dispatch({type: ADD_ERROR, payload: {status:500, message:res.stack}});
                }}
        });
    }
}

export function showForm(data) {
    return {
        type: data.type,
        payload: data.payload,
    }
}

export function showLoader(data) {
    return {
        type: data.type,
        payload: data.payload
    }
}

export function showError(data) {
    return {
        type: data.type,
        payload: data.payload
    };
}
//
// export function addDoc(data) {
//     return function (dispatch) {
//         dispatch(loading);
//         const Api = new API();
//         console.log(data);
//         dispatch(addDoc());
//         dispatch(loaded);
//     }
// }
export function successAlert(data) {
    return {type:data.type, payload:data}
}

// export function searchDoc2(url) {
//     return function (dispatch) {
//         console.log(dispatch);
//         console.log('DISP');
//         const res = new API().getDoc(url).then(
//             res => {
//                 return dispatch({
//                     type: FIND_DOC,
//                     payload: res
//                 })
//             })
//
//     }
// }