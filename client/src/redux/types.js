export const INCOME_FORM = 'SHOW_INCOME_FORM';
export const OUTCOME_FORM = 'SHOW_OUTCOME_FORM';
export const SHOW_FORM = 'SHOW_FORM';
export const FIND_DOC ="FIND_DOC";
export const HIDE_FORM = "HIDE_FORM";
export const SHOW_LOADER = "SHOW_LOADER";
export const HIDE_LOADER = "HIDE_LOADER";
export const ADD_ERROR = "ADD_ERROR";
export const DELETE_ERROR ="DELETE_ERROR";
export const ADD_DOC = "ADD_DOC";
export const ADD_MESSAGE = "ADD_MESSAGE";
export const DELETE_MESSAGE = "DELETE_MESSAGE";

