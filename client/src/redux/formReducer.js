import {HIDE_FORM,SHOW_FORM,INCOME_FORM,OUTCOME_FORM} from "./types";

const initialState = {
    incoming: {
        show:false
    }, outcome: {
        show:false
    }
};

export const formReducer = (state = initialState, action) => {
    switch (action.type) {
        case SHOW_FORM:{
            const show = {show:true};
            return Object.assign({},state, {[action.payload]:show});
        }
        case HIDE_FORM : {
            const hide = {show:false};
            return Object.assign({},state, {[action.payload]:hide});

            return Object.assign({}, state, {
                cartOpen: !state.cartOpen
            });
        }
    }
    return state
};