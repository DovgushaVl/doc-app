import {DELETE_MESSAGE, ADD_MESSAGE} from "./types";

const initialState = {
    message : false,
    data: {}
};
export const successReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_MESSAGE : {
            return {...state, message: true,data: action.payload}
        }
        case (DELETE_MESSAGE) : {
            return {...state, message:false, data: {}}
        }
    }
    return state;
};

