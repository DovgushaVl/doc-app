import {FIND_DOC} from "./types";

const initialState = {
    data:[]
};

export const findFormReducer = (state = initialState, action) => {

    switch (action.type) {
        case FIND_DOC:{
            return Object.assign({},state,{data:action.payload})
        }
    }
    return state
};