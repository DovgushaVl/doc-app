import dotenv from 'dotenv';

const envFound = dotenv.config();
console.log(envFound);
if (!envFound){
    throw new Error('no .env file');
}


export default {
    port:parseInt(process.env.PORT, 10),
    dataBaseUrl:process.env.MONGODB_URI
};
