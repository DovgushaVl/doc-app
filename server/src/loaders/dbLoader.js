import config from '../config/index';
import mongoose from 'mongoose';

export default async ()=>{
    try {
    const mongo = await mongoose.connect(config.dataBaseUrl, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });
    return mongo.connection.db;
    }
    catch (e) {
        throw new Error(e);
    }
}