// const express = require("./express");
import express from "./express";
import dbLoader from "./dbLoader";
export default async ({expressApp})=>{
    await express({app:expressApp});
    console.log('Express => ready');
    await dbLoader();
    console.log('Mongo => ready')
}