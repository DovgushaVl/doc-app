import routes from '../api/index'
import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import generalError from '../errors/index';
export default ({app})=>{

    app.use(cors());
    app.use(bodyParser.json());
    app.use('/',routes());
    app.use(generalError);

    // app.use(express.static("../client/build/"));
    // app.use(('/'),express.static("../client/build/"));

}
