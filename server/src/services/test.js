import newError from "../errors/Error";

export default function () {
    if (1 < 2) {
        throw new newError('222', 500);
    }
}