export default class newError
    extends Error{
    constructor(message,status) {
        super(message,status);
        this.status = status;
    }
}