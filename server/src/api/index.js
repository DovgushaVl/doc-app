import {Router} from 'express';
import homeRoute from './routes/homeRoute';
import findRoute from "./routes/findRoute";
import addIncome from "./routes/addIncome";
import error from '../errors/index';
import addOutcome from "./routes/addOutcome";
export default ()=> {
    const app = Router();
    homeRoute(app);
    findRoute(app);
    addIncome(app);
    addOutcome(app);
    return app;
}