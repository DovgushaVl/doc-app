import Router from 'express';
import incomingModel from "../../db/incomingModel";
import newError from "../../errors/Error";
import searchEntryNumber from "../../services/searchEntryNumber";

import test from "../../services/test";

const route = Router();
export default (app) => {
    app.use('/create', route);
    route.post('/incoming', async (req, res, next) => {
        try {
            const body = req.body;
            const document = incomingModel({...body});
            const entryNumber = (body.entryNumb);
            console.log(body);
            const trimedEndtryNumber = entryNumber.trim();
            await searchEntryNumber(trimedEndtryNumber);
            await document.save(function (err) {
                    try {
                        if (err) {
                            throw new newError(err.message, 400);
                        }
                        return res.json({
                            document, text: "Сохранено"
                        })
                    } catch (e) {
                        next(e);
                    }
                }
            )
        } catch (e) {
            next(e);
        }
    });

};