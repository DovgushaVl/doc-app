import Router from 'express';
import incomingModel from "../../db/incomingModel";
import docsFilterService from "../../services/docsFilterService";
import newError from "../../errors/Error";
import {anyRegisterSearch} from "../../services/regExp";

const route = Router();
export default (app) => {
    app.use('/search', route);
    route.get('/:type/text=:text', async (req, res, next) => {
        try {
            const fields = ['entryNumb',
                'registrNumber',
                'adressee',
                'description',
                'executor',
                'outputNumber'];
            const findDocsId = [0];
            let response = [];
            const data = req.params;
            const select = data.type;
            const searchField = data.text;
            const reg = anyRegisterSearch(searchField);
            // console.log(reg);
            const reg2 = new RegExp('.', 'g');
            console.log(reg);
            // const reg = new RegExp('' + "qwer", 'i');
            for (let field of fields) {
                const document = await incomingModel.find({[field]: [reg]});
                // const document = await incomingModel.find({outputNumber: "qwer"});
                console.log(document);
                // const document = await incomingModel.find({: [reg]});
                if (document.length !== 0) {
                    document.map(item => {
                        response.push(item);
                        console.log(item);
                    })
                }
            }
            const uniqueData = docsFilterService(response);
            // throw new newError('ASFASFASFSA',404);
            res.json(uniqueData);
        } catch (e) {
            next(e);
        }
    })
};
