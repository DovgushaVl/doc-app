import mongoose from 'mongoose';

const outcomingSchema = new mongoose.Schema({
    outputNumber: {
        type: String,
        required: true
    },
    adressee: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    executor: {
        type: String,
        required: true
    },
    replyOn : {type:String, default : ""}
});
export default mongoose.model('Outcoming', outcomingSchema);

const defaultState = {
    outputNumber: '',
    adressee: '',
    description: '',
    executor: '',
    replyOn: ''
};