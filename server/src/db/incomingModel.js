import mongoose from 'mongoose';

const incomingSchema = new mongoose.Schema({
    entryNumb: {
        type: String,
        required: true,
        unique: true
    },
    registrNumber: {
        type: String,
        required: true
    },
    adressee: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    executor: {
        type: String,
        required: true
    },
    outputNumber: {
        type: String,
        default: ''
    },


});

export default mongoose.model('Incoming', incomingSchema)